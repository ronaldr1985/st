# My fork of st
============================

st is an extremely fast, small and lightweight terminal emulator for X11.


## Requirements
------------
In order to build dwm you need the Xlib header files.


## Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install
    
		or
		
    sudo make clean install


## My configuration
-------------
My configuration has one patch applied:
Scrollback

The diff for this can be found on the suckless website.

 also make some configuration changes, so I've changed the font to Monospace and made the font size 13, I've also changed the padding size to 32.
